#![doc = include_str!("../README.md")]
use chrono::{DateTime, Duration, Local};
use clap::Parser;
use colored::Colorize;
use reqwest::blocking::Client;
use scraper::{Html, Selector};
use std::fs::OpenOptions;
use std::io::Write;
use url::{ParseError, Url};

#[derive(Parser, Debug)]
#[clap(about, version, author)]
struct Cli {
    /// URL to check
    url: String,
    #[clap(short, long, value_name = "URL")]
    /// URL(s) to ignore if found (full url); separate multiple URLs with a comma
    skip: Option<String>,
    #[clap(short, long)]
    /// Quick run - check links only on the URL you provide and not any subsequent links
    quick: bool,
}

struct GoodLink {
    url: Url,
    html: String,
}

impl GoodLink {
    fn new(url: Url, html: String) -> GoodLink {
        GoodLink { url, html }
    }
}

enum LinkStatus {
    Good(GoodLink),
    Bad,
}

#[derive(PartialEq, Clone)]
struct Link {
    referer: Url,
    link: Url,
}

impl Link {
    fn new(referer: Url, link: Url) -> Link {
        Link { referer, link }
    }
}

struct CheckError {
    link: Url,
    referer: Url,
    error: String,
}

impl CheckError {
    fn new(link: Url, referer: Url, error: String) -> CheckError {
        CheckError {
            link,
            referer,
            error,
        }
    }
}

/// get absolute link
fn get_abs_link(href: &str, referer: Url) -> String {
    // if link is already an absolute link, use as is
    if href.starts_with("http") || href.starts_with("www") {
        href.to_string()
    // convert relative to absolute link
    } else {
        referer.join(href).unwrap().to_string()
    }
}

/// get all links from one url's html response
fn get_links(referer: GoodLink) -> Result<Vec<Link>, ParseError> {
    let document = Html::parse_document(&referer.html);
    let selector = Selector::parse("a").unwrap();

    let mut links = vec![];

    // go through all "a" elements
    for element in document.select(&selector) {
        // if it has an href value, modify that value as necessary and push to links
        if let Some(mut v) = element.value().attr("href") {
            v = v.trim();
            // don't add mailto, tel, or relative anchor links
            if !v.starts_with("mailto:") && !v.starts_with('#') && !v.starts_with("tel:") {
                let link = Url::parse(&get_abs_link(v, referer.url.clone()))?;
                links.push(Link::new(referer.url.clone(), link));
            }
        }
    }

    Ok(links)
}

/// check status of individual link
fn check_link(url: &Url, client: &Client) -> Result<LinkStatus, reqwest::Error> {
    let res = client.get(url.as_ref()).send()?;
    match res.status() != reqwest::StatusCode::NOT_FOUND {
        true => {
            let good_link = GoodLink::new(res.url().clone(), res.text()?);
            Ok(LinkStatus::Good(good_link))
        }
        false => Ok(LinkStatus::Bad),
    }
}

fn main() {
    // parse arguments provided
    let args = Cli::parse();
    let mut url = args.url;

    // add https if user didn't, notify them (in case http is preferred)
    if !url.starts_with("http") {
        println!("Adding 'https://' to provided domain...");
        url = format!("https://{}", url);
    }

    // parse as Url, shadowing url
    let url = match Url::parse(&url) {
        Ok(url) => url,
        Err(e) => {
            eprintln!("Error: {}", e);
            return;
        }
    };

    // create filenames
    let domain = url.host_str().unwrap().trim_matches('"');
    let dt: DateTime<Local> = Local::now();
    let format = "%Y-%m-%d-%H-%M";
    let broken_links_csv = format!("{}_broken_links_{}.csv", domain, dt.format(format));
    let errors_csv = format!("{}_errors_{}.csv", domain, dt.format(format));

    println!("\nChecking links found in {:?}", url.to_string());
    if args.quick {
        println!("{}", "Note: this is a quick run.".yellow());
    }
    println!();

    // start the check
    let start: DateTime<Local> = Local::now();
    let mut unchecked_links: Vec<Link> = vec![Link::new(url.clone(), url)];
    let mut valid_links: Vec<Link> = vec![];
    let mut invalid_links: Vec<Link> = vec![];
    let mut errors: Vec<CheckError> = vec![];

    let mut count = 0;
    let mut total_count = 0;

    let client = Client::new();

    while let Some(content) = unchecked_links.pop() {
        count += 1;
        total_count += 1;

        // if we already know the link is valid, we've already fetched all links from it, so no
        // need to process it (but we can't skip known bad links, because we need their referer)
        //
        // this check is also done below to prevent known valid links from being added to
        // unchecked_links in the first place, but there may be instances of the same link being
        // added multiple times before the first instance of it can be checked, so we check again
        // here, where we are able to catch all subseqent occurrences after the first
        if valid_links.iter().any(|link| link.link == content.link) {
            continue;
        }

        // otherwise check it
        match check_link(&content.link, &client) {
            Ok(LinkStatus::Good(good_link)) => {
                println!("{} is {}", content.link, "OK".green());
                valid_links.push(content.clone());

                // only get links from the starting url if this is a "quick" run
                if args.quick && count > 1 {
                    continue;
                }

                // get further links if link is within domain we're interested in & not a file
                // TODO: probably a better way to check if a file than this
                if content.referer.host() == content.link.host()
                    && !&content.link.as_ref().ends_with(".pdf")
                    && !&content.link.as_ref().ends_with(".xls")
                    && !&content.link.as_ref().ends_with(".xlsx")
                    && !&content.link.as_ref().ends_with(".docx")
                    && !&content.link.as_ref().ends_with(".doc")
                    && !&content.link.as_ref().ends_with(".pptx")
                    && !&content.link.as_ref().ends_with(".twbx")
                    && !&content.link.as_ref().ends_with(".png")
                    && !&content.link.as_ref().ends_with(".jpg")
                    && !&content.link.as_ref().ends_with(".jpeg")
                {
                    match get_links(good_link) {
                        Ok(new_urls) => {
                            'outer: for new_url in new_urls {
                                // skip any urls specified by user
                                if let Some(ref skip_urls) = args.skip {
                                    for skip_url in skip_urls.split(',') {
                                        if new_url.link.as_ref() == skip_url {
                                            continue 'outer;
                                        }
                                    }
                                };
                                // limit, as much as possible, what gets added to unchecked_links
                                if !valid_links.contains(&new_url)
                                    && !invalid_links.contains(&new_url)
                                    && !unchecked_links.contains(&new_url)
                                {
                                    // if we already know the link is broken, add to invalid
                                    if invalid_links.iter().any(|link| link.link == new_url.link) {
                                        invalid_links.push(new_url.clone());
                                        total_count += 1;
                                    // if we already know the link is good, add to valid
                                    } else if valid_links
                                        .iter()
                                        .any(|link| link.link == new_url.link)
                                    {
                                        valid_links.push(new_url.clone());
                                        total_count += 1;
                                    } else {
                                        unchecked_links.push(new_url.clone());
                                    }
                                } else {
                                    total_count += 1;
                                }
                            }
                        }
                        Err(e) => {
                            eprintln!(
                                "{} getting links from {} (from referer {}): {}",
                                "Error".red(),
                                content.link,
                                content.referer,
                                e
                            );
                            errors.push(CheckError::new(
                                content.link.clone(),
                                content.referer.clone(),
                                e.to_string(),
                            ));
                        }
                    }
                }
            }
            Ok(LinkStatus::Bad) => {
                invalid_links.push(content.clone());
                println!("{} is {}", content.link, "Broken".red());
            }
            Err(e) => {
                eprintln!(
                    "{} checking {} (from referer {}): {}",
                    "Error".red(),
                    content.link,
                    content.referer,
                    e
                );
                errors.push(CheckError::new(
                    content.link.clone(),
                    content.referer.clone(),
                    e.to_string(),
                ));
            }
        }
        // TODO: possibly set optional max links to be checked? (prevent against infinite loop)
        // if count == 200 {
        //     break;
        // }
    }

    let elapsed: Duration = Local::now() - start;

    let human_elapsed: String;
    if elapsed.num_hours() > 0 {
        let hours = elapsed.num_hours();
        let minutes = elapsed.num_minutes() - elapsed.num_hours() * 60;
        human_elapsed = format!("{} hours, {} minutes", hours, minutes);
    } else if elapsed.num_minutes() > 0 {
        let minutes = elapsed.num_minutes();
        let seconds = elapsed.num_seconds() - elapsed.num_minutes() * 60;
        human_elapsed = format!("{} minutes, {} seconds", minutes, seconds,);
    } else {
        human_elapsed = format!("{} seconds", elapsed.num_seconds());
    }

    println!(
        "\nChecked {} links (verifying an approximate total of {}) in {}.",
        count, total_count, human_elapsed,
    );

    // process results
    if invalid_links.is_empty() {
        println!("No broken links found.");
    } else {
        println!("Broken links saved to {}", broken_links_csv);

        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .append(true)
            .create(true)
            .open(broken_links_csv)
            .expect("cannot open file");

        // write the header
        let content = "Referer (link on this page),Link\n".to_string();
        file.write_all(content.as_bytes()).expect("write failed");

        for link in invalid_links {
            let content = format!("{:?},{:?}", link.referer.as_ref(), link.link.as_ref());
            file.write_all(content.as_bytes()).expect("write failed");
            file.write_all("\n".as_bytes()).expect("write failed");
        }
    }

    // create separate csv of any errors from checking links
    if errors.is_empty() {
        println!("No errors occurred.");
    } else {
        println!("Errors saved to {}", errors_csv);
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .append(true)
            .create(true)
            .open(errors_csv)
            .expect("cannot open file");

        // write the header
        let content = "Referer (link on this page),Link,Error\n".to_string();
        file.write_all(content.as_bytes()).expect("write failed");

        for error in errors {
            let content = format!(
                "{:?},{:?},{}",
                error.referer.as_ref(),
                error.link.as_ref(),
                error.error
            );
            file.write_all(content.as_bytes()).expect("write failed");
            file.write_all("\n".as_bytes()).expect("write failed");
        }
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn abs_link_domain_referer_no_trailing_slash() {
        let referer = Url::parse("https://kdwarn.dev").unwrap();
        assert_eq!(
            get_abs_link("https://kdwarn.dev/hello/", referer),
            "https://kdwarn.dev/hello/"
        )
    }

    #[test]
    fn abs_link_domain_referer_trailing_slash() {
        let referer = Url::parse("https://kdwarn.dev/").unwrap();
        assert_eq!(
            get_abs_link("https://kdwarn.dev/hello/", referer),
            "https://kdwarn.dev/hello/"
        )
    }

    #[test]
    fn abs_link_www_domain_referer() {
        let referer = Url::parse("https://kdwarn.dev").unwrap();
        assert_eq!(
            get_abs_link("www.kdwarn.dev/hello/", referer),
            "www.kdwarn.dev/hello/"
        )
    }

    #[test]
    fn rel_link_with_slash_domain_referer_no_trailing_slash() {
        let referer = Url::parse("https://kdwarn.dev").unwrap();
        assert_eq!(get_abs_link("/hello", referer), "https://kdwarn.dev/hello")
    }

    #[test]
    fn rel_link_with_slash_domain_referer_trailing_slash() {
        let referer = Url::parse("https://kdwarn.dev/").unwrap();
        assert_eq!(get_abs_link("/hello", referer), "https://kdwarn.dev/hello")
    }

    #[test]
    fn rel_link_no_slash_domain_referer_no_trailing_slash() {
        let referer = Url::parse("https://kdwarn.dev").unwrap();
        assert_eq!(get_abs_link("hello", referer), "https://kdwarn.dev/hello")
    }

    #[test]
    fn rel_link_no_slash_domain_referer_trailing_slash() {
        let referer = Url::parse("https://kdwarn.dev/").unwrap();
        assert_eq!(get_abs_link("hello", referer), "https://kdwarn.dev/hello")
    }

    #[test]
    fn rel_link_no_slash_referer_with_subdir_trailing_slash() {
        let referer = Url::parse("https://kdwarn.dev/hello/").unwrap();
        assert_eq!(
            get_abs_link("kris", referer),
            "https://kdwarn.dev/hello/kris"
        )
    }

    #[test]
    fn rel_link_no_slash_referer_with_subdir_no_trailing_slash() {
        let referer = Url::parse("https://kdwarn.dev/hello/there").unwrap();
        assert_eq!(
            get_abs_link("kris", referer),
            "https://kdwarn.dev/hello/kris"
        )
    }

    #[test]
    fn rel_link_from_filename() {
        let referer = Url::parse("https://kdwarn.dev/hello/old.htm").unwrap();
        assert_eq!(
            get_abs_link("kris", referer),
            "https://kdwarn.dev/hello/kris"
        )
    }
}
